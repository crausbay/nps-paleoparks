library(tidyverse)


#This code creates a 'master' species list of all the sites in the analysis. 
#Shelley can then remove aquatic species, and other problematic species to create a list of species to keep in the analysis


#Data from Neotoma
site_check<-readRDS("nps_eastern_allsites_pollen_data_v2.RDS") %>% 
  dplyr::select(site.name, .id) %>% 
  distinct() %>% 
  rename(datast.id=.id) %>% 
  arrange(site.name)


#Data from Gregor

gs_files<-dir("./Gregor_pollen_datasets")

dat<-c()

for(i in 1:length(gs_files)){
  
  dat[[i]]<-read.csv(paste("./Gregor_pollen_datasets/", gs_files[[i]], sep=""))
  
}

gs_spp<-do.call("bind_rows", dat) %>% 
  gather(key= "species", value = "pollen_count", -1:-9) %>% 
  dplyr::select(species) %>% 
  distinct()

write.csv(site_check, "site_check_for_master_spp_list.csv")



spp_list<-readRDS("nps_eastern_allsites_pollen_data_v2.RDS") %>% 
  gather(key= "species", value = "pollen_count", -1:-10) %>% 
  dplyr::select(species) %>% 
  bind_rows(gs_spp) %>% 
  distinct() %>% 
  arrange(species) %>% 
  rename(Taxon_Name_Neotoma=species)


#Joining with harmonization table 

harm_table<-read.csv("Mottl_etal_NA_HarmonizationTable.csv") %>% 
  dplyr::select(2:7)


spp_list_w_harm_table<-left_join(spp_list, harm_table)


write.csv(spp_list_w_harm_table, "all_nps_eastern_paelo_sites_specieslist_with_harm_columns_updated.csv")


#### checking The Bowl which was added after this list was created ####


bowl<-read.csv("./Gregor_pollen_datasets/The Bowl_GS9_pollen_dat.csv") %>% 
  gather(key= "species", value = "pollen_count", -1:-9) %>% 
  dplyr::select(species) %>% 
  distinct() %>% 
  rename(Taxon_Name_Neotoma=species) %>% 
  left_join(spp_list_w_harm_table)

bowl_additional_spp_for_shelley<-bowl %>% 
  filter(is.na(NA_Agg1)==TRUE)


write.csv(bowl_additional_spp_for_shelley, "bowl_additional_spp_for_shelley.csv")


### Checking to see which species have cyperacea (it was initially missed for removal and need to know which sites should be re-standardized) ####

#Data from Gregor

gs_files<-dir("./Gregor_pollen_datasets")

dat<-c()

for(i in 1:length(gs_files)){
  
  dat[[i]]<-read.csv(paste("./Gregor_pollen_datasets/", gs_files[[i]], sep=""))
  
}

gs_spp<-do.call("bind_rows", dat) %>% 
  gather(key= "species", value = "pollen_count", -1:-9) %>% 
  dplyr::select(site.name, dataset.id, species) %>% 
  distinct()


#cominbing gregor's data with neotoma data and filter to just cyperacea spp. 
cyp_list<-readRDS("nps_eastern_allsites_pollen_data_v2.RDS") %>% 
  gather(key= "species", value = "pollen_count", -1:-10) %>% 
  rename(dataset.id=.id) %>% 
  dplyr::select(site.name, dataset.id, species) %>% 
  bind_rows(gs_spp) %>% 
  distinct() %>% 
  arrange(species) %>% 
  rename(Taxon_Name_Neotoma=species) %>% 
  filter(Taxon_Name_Neotoma=="Cyperaceae" | Taxon_Name_Neotoma=="Cyperaceae.undiff.") %>% 
  dplyr::select(-Taxon_Name_Neotoma) %>% 
  distinct() %>% 
  filter(dataset.id %in% site_check$datast.id)




