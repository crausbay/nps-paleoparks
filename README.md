# NPS PaleoParks

Exploration of paleorecords from eastern US National Parks to understand potential climate change impacts

# Scripts
## Scripts for pollen and geochronology dataset acquisition and processing 
#### NPS_paleo_data_extraction.R
This script interfaces with Neotoma to download the pollen/midden data for each site within the bounding box (all NPS boundaries east of the 100th meridian) that have a paleo record within the park, plus a buffer of XX around each park. It compiles the pollen data (including site.name and dataset.id) and also creates a .csv file of locations. 

#### Geochronology_extraction.R
This script is to extract the geochronology data (different from chronology controls) in order to compare errors w/chronology controls. 

#### site_chrono_data.R
This script pulls the chrono data for each site and creates a file that just contains depth data for each dataset as well. This is subsequently used for age/depth modeling. This dumps output into "./age_depth_dat/"

#### Core_top_database.R
Creates a database of datasets with core top errors (which are subsequently manually fixed) in the input files for the bulk-baconizing script below. 

#### Age_depth_models.R
This script runs a bacon model for an individual site, builds a posterior draws dataframe derived from that individual site’s Bacon run, and writes a csv file with posterior outputs for use in RoC analysis

#### Site_datasets.R
This script integrates new age depth models (from our own updated bacon runs), removes aquatic species from the records, and generates individual .csv files for each data set for running cluster analyses. It also creates a file for summarizing basal ages

### Scripts for analyzing and summarizing changes/transformations in pollen communities 
#### Cluster_analysis_percentages_all_sites.R
This script loops through each site and does the following: 
- converts all data to percentages
- creates a dissimilarity matrix using the ‘bray-curtis’ method using the function ‘vegdist’ from the package ‘vegan’
- runs a cluster analysis on the dissimilarity matrix using the method ‘coniss’ with the function ‘chclust’ from the ‘rioja’ package
- creates a dataframe with containing the ‘heights’ which describes the magnitude of ecological change. 
- Run a ‘broken stick’ model on each cluster analysis, which determines the number of distinct ecological groups and creates a database of number of groups for each dataset
- exports a stratigraphic plot with clusters for visualization 
- Creates a database of ‘degree of change’ to compare amongst sites
- Create a database of summary stats to compare amongst sites

#### Indicator_spp_analysis.R
This script integrates the breakpoint data and pollen data and uses the package ‘indicspecies’ to identify prominent vegetation types for each cluster for each site.

inputs: 
- breakpoint_files_eco_17_21

outputs: 
- llsites_indspp_120321.csv

#### Rate of change analysis
All input data for each site that goes into R-Ratepol 
- standardized community data sets: 00_CommunityData_standardized 
- age data: 00_SiteAges_Standardized 
- age uncertainty draws: 01_AgeUncertainty_Bacon_1000Draws 

Scripts:
- Rate of Change.R
- Community standardization script: 08_RoCPrep_CommunityStandardization.R 

#### Roc_transformation_plot.R
script to generate figures to compare peak rocs to transformations
inputs: 
- eco-17-21-sites_elevation_20211208.csv
- roc-peaks-annotated.csv
- full-roc-records.csv
- breakpoint_files_eco_17_21

outputs: 
- peak_roc_transformation_allsites_032322.png
- roc_transformation_allsites_panel_032322.png

#### Database of ecological transformations
inputs:
- breakpoint_files_eco_17_21
- Expert_opinion_bibliography.csv
- roc-peaks-annotated.csv

outputs:
- Transformation_RoC_Database.csv

### Scripts for incorporating future rates of change
Grab data from the Tree Atlas. Already organized by NPS units (78 of them)

Grab data from FIA 2000 (maybe part of Tree Atlas?)

Calculate RoC between FIA 2000 and Tree Atlas 2100 projections


