library(ggalluvial) # alluvial plots
library(tidyverse) # data wrangling; ggplot
library(RColorBrewer) # color palettes
library(patchwork) # combining plots
##### ALLUVIAL PLOTS #####

### Read in data
# File shared by SC on Google Drive (not modified)
vegtype <- read.csv("ExpertBreakpoints_2.14.20221.csv")
head(vegtype)


### Data clean up 
# check how many veg types are listed, and how many counts in each? 
table(vegtype$Before)
table(vegtype$After)

# fix a typo: 
vegtype %>% mutate(Before =str_replace_all(Before, "SpruceFIr", "SpruceFir")) -> vegtype

# make vegetation type a factor
vegtype %>% mutate(Before=as.factor(Before), After = as.factor(After)) -> vegtype

# make names nicer for plotting
levels(vegtype$Before) <- c("Aspen", "Douglas Fir", "Pine", "Sagebrush", "Spruce", "Spruce-Fir", "Spruce-Pine")
levels(vegtype$After) <- c("Aspen", "Douglas Fir", "Grass", "Pine", "Sagebrush", "Spruce", "Spruce-Fir", "Spruce-Pine", "Willow")

table(vegtype$Before)
table(vegtype$After)
# "After" has more unique vegetation types 

### create a custom ggplot theme for alluvial plots:
theme_alluvial <- function() {
  theme_bw() +
  theme(legend.position = "none", plot.title = element_text(hjust = 0.5, size = 14),
        axis.text.x  = element_text(size = 14), panel.grid = element_blank(),
        axis.title = element_text(size = 14),
        axis.line = element_blank(), axis.ticks = element_blank(), axis.title.y = element_blank(),
        axis.text.y = element_blank(), panel.border = element_blank())
}

### select colors for plotting:

# this code creates a dataframe with a row for each veg type, and an assigned color
# I will join this with our veg table later
colors <- data.frame(
  # column 1: lists the different types of veg
  VegType = unique(vegtype$After), 
  # column 2: lists a unique color for each vegetation type
  color = c("indianred2","lightblue4","lemonchiffon4","salmon2","aquamarine3","cyan4" ,"lightcyan3","brown3","chocolate"))
# arrange by alphabetic order of veg type
colors %>% arrange(VegType) -> colors
##############################
#### BEFORE: AFTER OVERALL ###
##############################

# Plot all sites together with all transition types

#### 1: Get data in "alluvial format":

# first count up the number of sites that show the same combo of before + after
vegtype %>% group_by(Before, After) %>% summarise(freq = n()) %>% ungroup() %>%
  # each row is a combination of before/after and then a count of how many transitions fit into that category
  # now give each of the rows a unique # (ggalluvial needs this for plotting)
  mutate(alluvium = 1:nrow(.)) %>% 
  # now make this data into long format
  # each row is a time period (Before or After) and a vegtype 
  # each row has the # of records within that category
  pivot_longer(Before:After, names_to = "Time", values_to = "VegType") %>% 
  # now make time into a numeric variable so "Before" is on the left hand side
  mutate(Time = case_when(Time == "Before"~0,
                          Time=="After"~1)) %>%
  # join with our colors dataframe for plotting
  left_join(colors) -> vegtype_alluvial

head(vegtype_alluvial)


#### 2: Create Alluvial Plot 
vegtype_alluvial %>% 
  ggplot(aes(x=as.factor(Time), stratum = VegType, alluvium = alluvium, y = freq, fill = color, label = VegType)) +
  geom_flow(alpha = .8) +
  geom_stratum(col = "gray", alpha = .3) +
  geom_text(stat = "stratum", hjust =0, nudge_x = -.1) +
  scale_fill_identity() +
  scale_x_discrete(labels = c("Before", "After"), name = "") +
  theme_alluvial() +
  ggtitle("All Sites") 

##############################
##### Plot by Region ######### 
##############################

#### 1: Get data in "alluvial format":
# this time with region included
vegtype_alluvial_ecoregion <- rbind(
  # southern rockies
  vegtype %>% filter(ecoregion == "Southern Rockies") %>% 
    group_by(Before, After) %>% summarise(freq = n()) %>% ungroup() %>%
    mutate(alluvium = 1:nrow(.), ecoregion = "Southern Rockies") %>% 
    pivot_longer(Before:After, names_to = "Time", values_to = "VegType") %>% 
    mutate(Time = case_when(Time == "Before"~0, Time=="After"~1)) %>% left_join(colors),
 # middle rockies
 vegtype %>% filter(ecoregion == "Middle Rockies") %>% 
   group_by(Before, After) %>% summarise(freq = n()) %>% ungroup() %>%
   mutate(alluvium = 1:nrow(.), ecoregion = "Middle Rockies") %>% 
   pivot_longer(Before:After, names_to = "Time", values_to = "VegType") %>% 
   mutate(Time = case_when(Time == "Before"~0,  Time=="After"~1)) %>% left_join(colors))

head(vegtype_alluvial_ecoregion)  

### 2: plot each ecoregion
(mr_alluvial <- vegtype_alluvial_ecoregion %>% filter(ecoregion == "Middle Rockies") %>% 
  ggplot(aes(x=as.factor(Time), stratum = VegType, alluvium = alluvium, y = freq, fill = color, label = VegType)) +
  geom_flow(alpha = .8, width =1/3) +
  geom_stratum(col = "gray", alpha = .3) +
  geom_text(stat = "stratum", hjust =0, nudge_x = -.1, size =3) +
  scale_fill_identity() +
  scale_x_discrete(labels = c("Before", "After"), name = "") +
  theme_alluvial() +
  ggtitle("Middle Rockies"))

ggsave(mr_alluvial, filename = "figures/Alluvial_MiddleRockies_allTransitions.pdf", height = 6, width = 6, units = "in")

(sr_alluvial <- vegtype_alluvial_ecoregion %>% filter(ecoregion == "Southern Rockies") %>% 
  ggplot(aes(x=as.factor(Time), stratum = VegType, alluvium = alluvium, y = freq, fill = color, label = VegType)) +
  geom_flow(alpha = .8, width = 1/3) +
  geom_stratum(col = "gray", alpha = .3) +
  geom_text(stat = "stratum", hjust =0, nudge_x = -.1, size = 3) +
  scale_fill_identity() +
  scale_x_discrete(labels = c("Before", "After"), name = "") +
  theme_alluvial() +
  ggtitle("Southern Rockies"))


ggsave(sr_alluvial, filename = "figures/Alluvial_SouthernRockies_allTransitions.pdf", height = 6, width = 6, units = "in")

###################################
##### Plot by Time Period Bin ##### 
###################################
# correct MR and SR Emerald lake
vegtype %>% mutate(site.name = case_when(
  site.name == "Emerald Lake" & ecoregion == "Middle Rockies"~"Emerald Lake (MR)",
  site.name == "Emerald Lake" & ecoregion == "Southern Rockies"~"Emerald Lake (SR)",
  TRUE~site.name
)) -> vegtype

# what were the time periods of transformations in the dataset?
vegtype %>% ggplot(aes(x=Mean.Age)) + geom_histogram(bins = 15, col = "black", fill = "goldenrod")

# create a dataframe arranged by the time period of transformation with 3 categories:
# (1) from 0 - 7000 years ago, (2) from 7000 - 12000 years ago, (3) more than 12000 years ago

vegtype %>%
  mutate(Age_bin = cut(Mean.Age, breaks = c(-1000, 7500, 21000), labels = c("7500 ybp - present", "21000 - 7500 ybp"))) %>%
  group_by(Before, After, Age_bin) %>% summarise(freq = n()) %>% ungroup() -> vegtype_bin

# split into diff datasets by time period
vegtype_bin %>% filter(Age_bin == levels(Age_bin)[1]) %>% mutate(subject = row_number()) %>% pivot_longer(Before:After, names_to = "Transition", values_to = "VegType") %>% mutate(Transition = factor(Transition, levels = c("Before", "After"))) %>% 
  arrange(subject) %>% left_join(colors) -> vegtype_bin_1

vegtype_bin %>% filter(Age_bin == levels(Age_bin)[2]) %>% mutate(subject = row_number()) %>% pivot_longer(Before:After, names_to = "Transition", values_to = "VegType") %>% mutate(Transition = factor(Transition, levels = c("Before", "After"))) %>% 
  arrange(subject) %>% left_join(colors) -> vegtype_bin_2


(vegtype_bin_1%>%
  ggplot(aes(x=Transition, stratum = as.factor(VegType), alluvium = subject, y = freq, fill = color, label = VegType)) +
  geom_flow(na.rm = TRUE) + 
  geom_stratum(na.rm = TRUE) +
  geom_text(stat = "stratum", hjust =0, nudge_x = -.1, size =4) +
  theme_alluvial() +
  scale_fill_identity() +
  scale_x_discrete(limits = c("Before", "After"), expand = c(0.12,0.12)) +
  theme(legend.position = "bottom", axis.line = element_line(), axis.ticks = element_line(), axis.title.y = element_text(angle = 90, size = 14),
        axis.text.y = element_text(size = 14), legend.title = element_text(size = 14)) +
  xlab("Time period") +
  ylab("Frequency") +
  ggtitle("7,500 ybp - present") -> bin1)

(vegtype_bin_2%>%
  ggplot(aes(x=Transition, stratum = as.factor(VegType), alluvium = subject, y = freq, fill = color, label = VegType)) +
  geom_flow(na.rm = TRUE) + 
  geom_stratum(na.rm = TRUE, width = 1/3) +
  geom_text(stat = "stratum", hjust =0, nudge_x = -.1, size =4) +
  theme_alluvial() +
  scale_fill_identity() +
  scale_x_discrete(limits = c("Before", "After"), expand = c(0.12,0.12)) +
  theme(legend.position = "bottom", axis.line = element_line(), axis.ticks = element_line(), axis.title.y = element_text(angle = 90, size = 14),
        axis.text.y = element_text(size = 14), legend.title = element_text(size = 14)) +
  xlab("Time period") +
  ylab("Frequency") +
  ggtitle("21,000 ybp - 7,500 ybp") -> bin2)


bin2 + bin1 + plot_layout(guides = "collect")

###########################################
##### Plot all transitions for a site ##### 
###########################################

# get data into long format 
vegtype %>% select(site.name, ecoregion, Age = age...bottom, VegType = Before) -> vegtype_time

# add in the last transition by hand
vegtype %>% group_by(site.name, ecoregion) %>% arrange(site.name, desc(age...top)) %>% 
  mutate(rank = rank(age...top)) %>% filter(rank == 1) %>% # get the last sample (rank = 1)
  ungroup() %>% 
  select(site.name, ecoregion, Age = age...top, VegType = After) %>% rbind(vegtype_time) %>% 
  arrange(site.name, ecoregion, desc(Age)) -> vegtype_time

head(vegtype_time)
# now our variable is the first appearance of a vegetation type (sort of)

vegtype_time %>% group_by(site.name, ecoregion) %>% arrange(Age) %>% mutate(time_rank = rank(-Age)) %>% 
  ungroup() %>% arrange(site.name, ecoregion, time_rank) -> vegtype_time

# plot all the transitions regardless of time...
vegtype_time %>% mutate(VegType = as.factor(VegType)) %>%
  ggplot(aes(x=time_rank, stratum = VegType, alluvium = site.name, fill = VegType,
             label = VegType))+
  scale_fill_manual(values = c(colors$color)) +
  geom_flow(stat = "alluvium", lode.guidance = "frontback", na.rm=TRUE) +
  geom_stratum(na.rm=TRUE) +
  theme(legend.position = "bottom") +
  scale_x_continuous(breaks = c(1:5), name= "Transition Number") +
  theme_alluvial() +
  theme(legend.position = "bottom") 
# the first transition for each site is usually from sagebrush to pine, spruce, or spruce-fir
# the second transition is often to pine from spruce or spruce-fir
# this visualization lets you track each site

# aggregate the data for a simpler visualization
# instead of a line per site we want the similar transitions grouped
vegtype_time %>% pivot_wider(names_from = time_rank, values_from = VegType, id_cols = c("site.name", "ecoregion"), names_prefix = "t") %>% 
  group_by(t1, t2, t3, t4, t5) %>% summarise(freq = n()) %>% ungroup() %>% mutate(subject = row_number()) %>% 
  pivot_longer(t1:t5, names_to = "Transition", values_to = "VegType") %>% arrange(subject) -> vegtype_time_grouped
  
vegtype_time_grouped %>%
  ggplot(aes(x=Transition, stratum = as.factor(VegType), alluvium = subject, y = freq, fill = VegType)) +
  geom_flow(na.rm = TRUE) + 
  geom_stratum(na.rm = TRUE) +
  scale_fill_manual(values = colors$color) +
  theme_alluvial() +
  theme(legend.position = "bottom", axis.line = element_line(), axis.ticks = element_line(), axis.title.y = element_text(angle = 90, size = 14),
        axis.text.y = element_text(size = 14), legend.title = element_text(size = 14)) +
  xlab("Transition number") +
  ylab("Frequency")




